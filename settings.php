<?php

/**
 * DRM Social by Middag
 *
 * @package     mod_drmsocial
 * @copyright   2021 Middag Tecnologia (https://www.middag.com.br)
 * @author      Michael Meneses <michael@michaelmeneses.com.br>
 * @license     Commercial
 */

defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {

    //--- modedit defaults -----------------------------------------------------------------------------------
    $settings->add(new admin_setting_heading('drmsocialmodeditdefaults',
        get_string('modeditdefaults', 'admin'),
        get_string('condifmodeditdefaults', 'admin')));

    $settings->add(new admin_setting_configselect('drmsocial/textlicense',
        get_string('textlicense', 'drmsocial'),
        get_string('textlicense_help', 'drmsocial'),
        1,
        [
            1 => get_string('textlicense_option1', 'mod_drmsocial'),
            2 => get_string('textlicense_option2', 'mod_drmsocial'),
            3 => get_string('textlicense_optioncustom', 'mod_drmsocial'),
        ]));

    $settings->add(new admin_setting_configtext('drmsocial/textlicensecustom',
        get_string('textlicensecustom', 'drmsocial'),
        get_string('textlicensecustom_help', 'drmsocial'),
        '',
        PARAM_RAW));
}
