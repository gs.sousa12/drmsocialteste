<?php

/**
 * DRM Social by Middag
 *
 * @package     mod_drmsocial
 * @copyright   2021 Middag Tecnologia (https://www.middag.com.br)
 * @author      Michael Meneses <michael@michaelmeneses.com.br>
 * @license     Commercial
 */

namespace mod_drmsocial\output;

defined('MOODLE_INTERNAL') || die();

class mobile
{
    /**
     * Returns the initial page when viewing the activity for the mobile app.
     *
     * @param array $args Arguments from tool_mobile_get_content WS
     * @return array HTML, javascript and other data
     */
    public static function mobile_view_activity($args)
    {
        global $OUTPUT, $DB, $USER;

        $args = (object)$args;

        $cmid = $args->cmid;

        $cm = get_coursemodule_from_id('drmsocial', $cmid);
        $context = \context_module::instance($cm->id);
        self::require_capability($cm, $context, 'mod/drmsocial:view');

        $drmsocial = $DB->get_record('drmsocial', ['id' => $cm->instance]);

        $fileurl = new \moodle_url('/mod/drmsocial/mobile/pluginfile.php', ['id' => $drmsocial->id, 'userid' => $USER->id]);
        $fileurl = $fileurl->out(true);

        $fs = get_file_storage();
        $files = $fs->get_area_files($context->id, 'mod_drmsocial', 'content');
        $filename = '';
        foreach ($files as $file) {
            if ($file && !$file->is_directory()) {
                $filename = $file->get_filename();
            }
        }

        $data = [
            'drmsocial' => $drmsocial,
            'fileurl' => $fileurl,
            'filename' => $filename,
            'currenttimestamp' => time()
        ];

        return [
            'templates' => [
                [
                    'id' => 'main',
                    'html' => $OUTPUT->render_from_template('mod_drmsocial/mobile_view_activity_page', $data),
                ],
            ],
            'javascript' => '',
            'otherdata' => ''
        ];
    }

    /**
     * Confirms the user is logged in and has the specified capability.
     *
     * @param \stdClass $cm
     * @param \context $context
     * @param string $cap
     */
    protected static function require_capability(\stdClass $cm, \context $context, string $cap)
    {
        require_login($cm->course, false, $cm, true, true);
        require_capability($cap, $context);
    }
}
