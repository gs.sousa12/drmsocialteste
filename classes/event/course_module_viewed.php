<?php

/**
 * DRM Social by Middag
 *
 * @package     mod_drmsocial
 * @copyright   2021 Middag Tecnologia (https://www.middag.com.br)
 * @author      Michael Meneses <michael@michaelmeneses.com.br>
 * @license     Commercial
 */

namespace mod_drmsocial\event;

defined('MOODLE_INTERNAL') || die();

class course_module_viewed extends \core\event\course_module_viewed
{
    protected function init()
    {
        $this->data['objecttable'] = 'drmsocial';
        parent::init();
    }
}
