<?php

/**
 * DRM Social by Middag
 *
 * @package     mod_drmsocial
 * @copyright   2021 Middag Tecnologia (https://www.middag.com.br)
 * @author      Michael Meneses <michael@michaelmeneses.com.br>
 * @license     Commercial
 */

namespace mod_drmsocial;

defined('MOODLE_INTERNAL') || die();

class drmsocial extends model
{
    public $id;
    public $name;
    public $course;
    public $intro;
    public $introformat;
    public $textlicense;
    public $textlicensecustom;
    public $revision;

    protected $table = 'drmsocial';
}
