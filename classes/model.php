<?php

/**
 * DRM Social by Middag
 *
 * @package     mod_drmsocial
 * @copyright   2021 Middag Tecnologia (https://www.middag.com.br)
 * @author      Michael Meneses <michael@michaelmeneses.com.br>
 * @license     Commercial
 */

namespace mod_drmsocial;

defined('MOODLE_INTERNAL') || die;

abstract class model
{
    const PERPAGE = 1000;

    public $timecreated;
    public $timemodified;

    function __construct($id = null)
    {
        if (!is_null($id) && $id > 0) {
            if ($object = self::get($id)) {
                foreach ($object as $key => $value) {
                    $this->$key = $value;
                }
            }
        }
    }

    public function getPath()
    {
        $path = explode('\\', __CLASS__);
        return array_pop($path);
    }

    public function getTable()
    {
        if (!isset($this->table)) {
            return self::getPath();
        }

        return $this->table;
    }

    protected function get($id)
    {
        global $DB;

        return $DB->get_record($this->getTable(), ['id' => $id]);
    }

    protected function save()
    {
        global $DB;

        foreach ($this as $key => $value) {
            if (strpos($key, '_editor') !== false) {
                $key = str_replace('_editor', '', $key);
            }
            if (is_array($value) && isset($value['text'])) {
                $this->$key = $value['text'];
                $format = $key . "format";
                if (is_null($this->contentformat)) {
                    $this->$format = $value['format'];
                }
            }
        }

        if (isset($this->id) && $this->id > 0) {
            $this->timemodified = time();
            $DB->update_record($this->getTable(), $this);
        } else {
            $this->timecreated = time();
            $this->timemodified = time();
            $this->id = $DB->insert_record($this->getTable(), $this);
        }

        return $this;
    }

    public function update($formdata = [])
    {
        foreach ($formdata as $key => $value) {
            if (strpos($key, '_editor') !== false) {
                $key = str_replace('_editor', '', $key);
            }
            if (property_exists($this, $key)) {
                $this->$key = $value;
            }
        }

        return $this->save();
    }

    public function delete()
    {
        global $DB;

        return $DB->delete_records($this->getTable(), ['id' => $this->id]);
    }


    public function all()
    {
        global $DB;

        return $DB->get_records($this->getTable());
    }

    public function get_items($searchcriteria, $page, &$totalcount, $sort = '')
    {
        global $DB;

        $searchcond = [];
        $params = [];
        $i = 0;
        foreach ($searchcriteria as $field => $searchterm) {
            $i++;
            if (is_array($searchterm)) {
                list($insql, $inparams) = $DB->get_in_or_equal($searchterm, SQL_PARAMS_NAMED);
                $searchcond[] = "$field $insql";
                $params = array_merge($inparams, $params);
            } else if (is_numeric($searchterm)) {
                $params['ss' . $i] = $searchterm;
                $searchcond[] = $DB->sql_like($field, ":ss$i", false, true);
            } else {
                $params['ss' . $i] = "%$searchterm%";
                $searchcond[] = $DB->sql_like($field, ":ss$i", false, true);
            }
        }

        if (empty($searchcond)) {
            $searchcond = array('1 = 1');
        }

        $searchcond = implode(" AND ", $searchcond);

        $data = [];
        $limitfrom = $limitto = 0;
        // TODO limit sql
        // $limitfrom = $page * self::PERPAGE;
        // $limitto = self::PERPAGE;

        $rs = $DB->get_recordset_select($this->getTable(), $searchcond, $params, $sort, '*', $limitfrom, $limitto);
        foreach ($rs as $record) {
            if (isset($record->id)) {
                $data[] = new static($record->id);
            } else {
                $data[] = $record->id;
            }
        }
        $rs->close();

        $totalcount = $n;

        return $data;
    }
}
