<?php

/**
 * DRM Social by Middag
 *
 * @package     mod_drmsocial
 * @copyright   2021 Middag Tecnologia (https://www.middag.com.br)
 * @author      Michael Meneses <michael@michaelmeneses.com.br>
 * @license     Commercial
 */

defined('MOODLE_INTERNAL') || die;

require_once($CFG->dirroot . '/mod/drmsocial/backup/moodle2/backup_drmsocial_stepslib.php');

class backup_drmsocial_activity_task extends backup_activity_task
{
    protected function define_my_settings()
    {
        // No specific settings for this activity.
    }

    protected function define_my_steps()
    {
        $this->add_step(new backup_drmsocial_activity_structure_step('drmsocial_structure', 'drmsocial.xml'));
    }

    static public function encode_content_links($content)
    {
        global $CFG;

        $base = preg_quote($CFG->wwwroot, "/");

        // Link to the list of drmsocials.
        $search = "/(" . $base . "\/mod\/drmsocial\/index.php\?id\=)([0-9]+)/";
        $content = preg_replace($search, '$@DRMSOCIALINDEX*$2@$', $content);

        // Link to drmsocial view by moduleid.
        $search = "/(" . $base . "\/mod\/drmsocial\/view.php\?id\=)([0-9]+)/";
        $content = preg_replace($search, '$@DRMSOCIALVIEWBYID*$2@$', $content);

        return $content;
    }
}
