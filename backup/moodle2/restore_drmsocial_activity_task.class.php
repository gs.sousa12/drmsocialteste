<?php

/**
 * DRM Social by Middag
 *
 * @package     mod_drmsocial
 * @copyright   2021 Middag Tecnologia (https://www.middag.com.br)
 * @author      Michael Meneses <michael@michaelmeneses.com.br>
 * @license     Commercial
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/mod/drmsocial/backup/moodle2/restore_drmsocial_stepslib.php');

class restore_drmsocial_activity_task extends restore_activity_task
{
    protected function define_my_settings()
    {
        // No particular settings for this activity.
    }

    protected function define_my_steps()
    {
        $this->add_step(new restore_drmsocial_activity_structure_step('drmsocial_structure', 'drmsocial.xml'));
    }

    static public function define_decode_contents()
    {
        $contents = array();
        $contents[] = new restore_decode_content('drmsocial', array('intro'), 'drmsocial');
        return $contents;
    }

    static public function define_decode_rules()
    {
        $rules = array();
        $rules[] = new restore_decode_rule('DRMSOCIALVIEWBYID', '/mod/drmsocial/view.php?id=$1', 'course_module');
        $rules[] = new restore_decode_rule('DRMSOCIALINDEX', '/mod/drmsocial/index.php?id=$1', 'course');
        return $rules;
    }

    static public function define_restore_log_rules()
    {
        $rules = array();
        $rules[] = new restore_log_rule('drmsocial', 'add', 'view.php?id={course_module}', '{drmsocial}');
        $rules[] = new restore_log_rule('drmsocial', 'update', 'view.php?id={course_module}', '{drmsocial}');
        $rules[] = new restore_log_rule('drmsocial', 'view', 'view.php?id={course_module}', '{drmsocial}');
        return $rules;
    }

    static public function define_restore_log_rules_for_course()
    {
        $rules = array();
        $rules[] = new restore_log_rule('drmsocial', 'view all', 'index.php?id={course}', null);
        return $rules;
    }
}
