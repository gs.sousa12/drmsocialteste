<?php

/**
 * DRM Social by Middag
 *
 * @package     mod_drmsocial
 * @copyright   2021 Middag Tecnologia (https://www.middag.com.br)
 * @author      Michael Meneses <michael@michaelmeneses.com.br>
 * @license     Commercial
 */

defined('MOODLE_INTERNAL') || die();

class restore_drmsocial_activity_structure_step extends restore_activity_structure_step
{
    protected function define_structure()
    {
        $paths = array();
        $paths[] = new restore_path_element('drmsocial', '/activity/drmsocial');

        // Return the paths wrapped into standard activity structure.
        return $this->prepare_activity_structure($paths);
    }

    protected function process_drmsocial($data)
    {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->course = $this->get_courseid();

        $data->timecreated = $this->apply_date_offset($data->timecreated);
        $data->timemodified = $this->apply_date_offset($data->timemodified);

        // Insert the drmsocial record.
        $newitemid = $DB->insert_record('drmsocial', $data);

        // Immediately after inserting "activity" record, call this.
        $this->apply_activity_instance($newitemid);
    }

    protected function after_execute()
    {
        $this->add_related_files('mod_drmsocial', 'content', null);
    }
}
