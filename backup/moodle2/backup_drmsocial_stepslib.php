<?php

/**
 * DRM Social by Middag
 *
 * @package     mod_drmsocial
 * @copyright   2021 Middag Tecnologia (https://www.middag.com.br)
 * @author      Michael Meneses <michael@michaelmeneses.com.br>
 * @license     Commercial
 */

defined('MOODLE_INTERNAL') || die;

class backup_drmsocial_activity_structure_step extends backup_activity_structure_step
{
    protected function define_structure()
    {
        // Define each element separated.
        $drmsocial = new backup_nested_element('drmsocial', array('id'),
            array('name', 'course', 'intro', 'introformat', 'textlicense', 'textlicensecustom', 'revision', 'timecreated', 'timemodified'));

        // Define sources.
        $drmsocial->set_source_table('drmsocial', array('id' => backup::VAR_ACTIVITYID));

        // Define file annotations.
        $drmsocial->annotate_files('mod_drmsocial', 'content', null);

        // Return the root element (drmsocial), wrapped into standard
        // activity structure.
        return $this->prepare_activity_structure($drmsocial);
    }
}
