<?php

/**
 * DRM Social by Middag
 *
 * @package     mod_drmsocial
 * @copyright   2021 Middag Tecnologia (https://www.middag.com.br)
 * @author      Michael Meneses <michael@michaelmeneses.com.br>
 * @license     Commercial
 */

defined('MOODLE_INTERNAL') || die();

$plugin->version  = 2021081701;
$plugin->requires = 2019111800;
$plugin->component = 'mod_drmsocial';
$plugin->maturity = MATURITY_STABLE;
$plugin->release  = '2.4.1';
$plugin->cron     = 0;
