<?php

/**
 * DRM Social by Middag
 *
 * @package     mod_drmsocial
 * @copyright   2021 Middag Tecnologia (https://www.middag.com.br)
 * @author      Michael Meneses <michael@michaelmeneses.com.br>
 * @license     Commercial
 */

require('../../config.php');

$id = required_param('id', PARAM_INT); // Course id.

$course = $DB->get_record('course', array('id' => $id), '*', MUST_EXIST);

require_course_login($course, true);
$PAGE->set_pagelayout('incourse');

$strdrmsocial = get_string('modulename', 'drmsocial');
$strdrmsocials = get_string('modulenameplural', 'drmsocial');
$strsectionname = get_string('sectionname', 'format_' . $course->format);
$strname = get_string('name');
$strintro = get_string('moduleintro');
$strlastmodified = get_string('lastmodified');

$PAGE->set_url('/mod/drmsocial/index.php', array('id' => $course->id));
$PAGE->set_title($course->shortname . ': ' . $strdrmsocials);
$PAGE->set_heading($course->fullname);
$PAGE->navbar->add($strdrmsocials);
echo $OUTPUT->header();

if (!$drmsocials = get_all_instances_in_course('drmsocial', $course)) {
    notice(get_string('thereareno', 'moodle', $strdrmsocials), "$CFG->wwwroot/course/view.php?id=$course->id");
    exit;
}

$usesections = course_format_uses_sections($course->format);

$table = new html_table();
$table->attributes['class'] = 'generaltable mod_index';

if ($usesections) {
    $table->head = array($strsectionname, $strname, $strintro);
    $table->align = array('center', 'left', 'left');
} else {
    $table->head = array($strlastmodified, $strname, $strintro);
    $table->align = array('left', 'left', 'left');
}

$modinfo = get_fast_modinfo($course);
$currentsection = '';
foreach ($drmsocials as $drmsocial) {
    $cm = $modinfo->cms[$drmsocial->coursemodule];
    if ($usesections) {
        $printsection = '';
        if ($drmsocial->section !== $currentsection) {
            if ($drmsocial->section) {
                $printsection = get_section_name($course, $drmsocial->section);
            }
            if ($currentsection !== '') {
                $table->data[] = 'hr';
            }
            $currentsection = $drmsocial->section;
        }
    } else {
        $printsection = '<span class="smallinfo">' . userdate($drmsocial->timemodified) . "</span>";
    }

    $extra = empty($cm->extra) ? '' : $cm->extra;
    $icon = '';
    if (!empty($cm->icon)) {
        // Each drmsocial has an icon in 2.0.
        $icon = '<img src="' . $OUTPUT->pix_url($cm->icon) . '" class="activityicon" alt="' . get_string('modulename', $cm->modname) . '" /> ';
    }
    // Dim hidden modules.
    $class = $drmsocial->visible ? '' : 'class="dimmed"';
    $table->data[] = array($printsection, "<a $class $extra href=\"view.php?id=$cm->id\">" . $icon . format_string($drmsocial->name) . "</a>", format_module_intro('drmsocial', $drmsocial, $cm->id));
}

echo html_writer::table($table);

echo $OUTPUT->footer();
