<?php

/**
 * DRM Social by Middag
 *
 * @package     mod_drmsocial
 * @copyright   2021 Middag Tecnologia (https://www.middag.com.br)
 * @author      Michael Meneses <michael@michaelmeneses.com.br>
 * @license     Commercial
 */

/**
 * AJAX_SCRIPT - exception will be converted into JSON.
 */
define('AJAX_SCRIPT', true);

/**
 * NO_MOODLE_COOKIES - we don't want any cookie.
 */
define('NO_MOODLE_COOKIES', true);

require_once('../../../config.php');
require_once('../lib.php');
require_once($CFG->libdir . '/filelib.php');
require_once($CFG->libdir . '/completionlib.php');
require_once($CFG->dirroot . '/webservice/lib.php');

// Allow CORS requests.
header('Access-Control-Allow-Origin: *');

// Authenticate the user.
$token = required_param('token', PARAM_ALPHANUM);
$drmsocialid = required_param('id', PARAM_INT);
$userid = required_param('userid', PARAM_INT);

$webservicelib = new webservice();
$authenticationinfo = $webservicelib->authenticate_user($token);

// Check the service allows file download.
$enabledfiledownload = (int) ($authenticationinfo['service']->downloadfiles);
if (empty($enabledfiledownload)) {
    throw new webservice_access_exception('Web service file downloading must be enabled in external service settings');
}

$cm = get_coursemodule_from_instance('drmsocial', $drmsocialid, 0, false, MUST_EXIST);
$course = $DB->get_record('course', array('id' => $cm->course), '*', MUST_EXIST);
$drmsocial = $DB->get_record('drmsocial', ['id' => $drmsocialid], '*', MUST_EXIST);

// Capabilities check.
require_capability('mod/drmsocial:view', \context_module::instance($cm->id));

// Set the custom certificate as viewed.
$completion = new completion_info($course);
$completion->set_module_viewed($cm);

$fs = get_file_storage();
$context = \context_module::instance($cm->id);
$files = $fs->get_area_files($context->id, 'mod_drmsocial', 'content');
$filename = '';
foreach ($files as $file) {
    if ($file && !$file->is_directory()) {
        $filename = $file->get_filename();
    }
}

$filearea = 'content';
$args = [
    0 => 1,
    1 => $filename,
];
$forcedownload = true;

drmsocial_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload);

exit();
