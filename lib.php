<?php

/**
 * DRM Social by Middag
 *
 * @package     mod_drmsocial
 * @copyright   2021 Middag Tecnologia (https://www.middag.com.br)
 * @author      Michael Meneses <michael@michaelmeneses.com.br>
 * @license     Commercial
 */

defined('MOODLE_INTERNAL') || die();

function drmsocial_supports($feature)
{
    switch ($feature) {
        case FEATURE_BACKUP_MOODLE2:
            return true;
        case FEATURE_COMPLETION_TRACKS_VIEWS:
            return true;
        case FEATURE_GRADE_HAS_GRADE:
            return false;
        case FEATURE_GRADE_OUTCOMES:
            return false;
        case FEATURE_GROUPINGS:
            return false;
        case FEATURE_GROUPS:
            return false;
        case FEATURE_MOD_ARCHETYPE:
            return MOD_ARCHETYPE_RESOURCE;
        case FEATURE_MOD_INTRO:
            return true;
        case FEATURE_SHOW_DESCRIPTION:
            return true;
        default:
            return null;
    }
}

function drmsocial_add_instance($data, $mform = null)
{
    $drmsocial = new \mod_drmsocial\drmsocial();

    $object = $drmsocial->update($data);

    drmsocial_save_files($data);

    return $object->id;
}

function drmsocial_update_instance($data, $mform = null)
{
    $drmsocial = new \mod_drmsocial\drmsocial();

    $drmsocial->id = $data->instance;
    $data->revision = $data->revision + 1;
    $object = $drmsocial->update($data);

    drmsocial_save_files($data);

    return $object;
}

function drmsocial_save_files($data)
{
    global $CFG, $USER;


    $fs = get_file_storage();
    $context = \context_module::instance($data->coursemodule);

    if ($data->file) {
        $usercontext = context_user::instance($USER->id);

        $draftfiles = $fs->get_area_files($usercontext->id, 'user', 'draft', $data->file, 'id');

        $file = null;
        if (count($draftfiles)) {
            foreach ($draftfiles as $draftfile) {
                if (!$draftfile->is_directory()) {
                    $file = $draftfile;
                }
            }

            try {
                require_once(dirname(__FILE__) . '/vendor/autoload.php');

                if (drmsocial_pdftk_require()) {
                    if ($file instanceof stored_file) {
                        $path = $fs->get_file_system()->get_local_path_from_storedfile($file, false);

                        $pdfOptions = drmsocial_pdftk_options();
                        $tempdir = drmsocial_temp_dir();

                        $pdf = new \mikehaertl\pdftk\Pdf($path, $pdfOptions);
                        $pdf->tempDir = $tempdir;
                        $pdf->allow('AllFeatures');
                        $content = $pdf->toString();

                        $fileinfo = [
                            'contextid' => $usercontext->id,
                            'component' => 'user',
                            'filearea' => 'draft',
                            'itemid' => file_get_unused_draft_itemid(),
                            'filepath' => '/',
                            'filename' => $file->get_filename(),
                            'mimetype' => $file->get_mimetype(),
                            'source' => $file->get_source(),
                        ];

                        $newfile = $fs->create_file_from_string($fileinfo, $content);

                        file_overwrite_existing_draftfile($newfile, $file);
                    }
                }
            } catch (Exception $e) {
                debugging($e->getMessage());
            }
        }

        $options = array('subdirs' => false);
        file_save_draft_area_files($data->file, $context->id, 'mod_drmsocial', 'content', 0, $options);
    }
}

function drmsocial_delete_instance($id)
{
    $cm = get_coursemodule_from_instance('drmsocial', $id, 0, false, MUST_EXIST);
    $context = \context_module::instance($cm->id);

    $drmsocial = new \mod_drmsocial\drmsocial($id);

    $fs = get_file_storage();
    if (!$fs->delete_area_files($context->id)) {
        return false;
    }

    $drmsocial->delete();

    return true;
}

function drmsocial_get_coursemodule_info($coursemodule)
{
    global $PAGE, $DB, $OUTPUT, $CFG;

    $dbparams = array('id' => $coursemodule->instance);
    $fields = 'id, name, intro, introformat, revision';

    if (!$drmsocial = $DB->get_record('drmsocial', $dbparams, $fields)) {
        return false;
    }

    $result = new cached_cm_info();
    $result->name = $drmsocial->name;
    $result->content = '';

    if ($coursemodule->showdescription) {
        // Convert intro to html.
        // Do not filter cached version, filters run at display time.
        $result->content = format_module_intro('drmsocial', $drmsocial, $coursemodule->id, false);
    }

    return $result;
}

function drmsocial_get_view_actions()
{
    return array('view', 'view help');
}

function drmsocial_get_post_actions()
{
    return array('update', 'add');
}

function drmsocial_get_file_areas($course, $cm, $context)
{
    return [
        'content' => get_string('content'),
    ];
}

function drmsocial_get_file_info($browser, $areas, $course, $cm, $context, $filearea, $itemid, $filepath, $filename)
{
    global $CFG;

    if ($context->contextlevel != CONTEXT_MODULE) {
        return null;
    }

    // Filearea must contain a real area.
    if (!isset($areas[$filearea])) {
        return null;
    }

    if (!has_capability('moodle/course:managefiles', $context)) {
        // Students can not peek here!
        return null;
    }

    $fs = get_file_storage();
    if ($filearea === 'uploaded') {
        $filepath = is_null($filepath) ? '/' : $filepath;
        $filename = is_null($filename) ? '.' : $filename;

        if (!$storedfile = $fs->get_file($context->id, 'mod_drmsocial', $filearea, 0, $filepath, $filename)) {
            // Not found.
            return null;
        }

        $urlbase = $CFG->wwwroot . '/pluginfile.php';

        return new file_info_stored($browser, $context, $storedfile, $urlbase, $areas[$filearea], false, true, true, false);
    }

    // Not found.
    return null;
}

function drmsocial_pluginfile($course, $cm, $context, $filearea, array $args, $forcedownload, array $options = array())
{
    global $CFG, $DB, $USER;

    if ($context->contextlevel != CONTEXT_MODULE) {
        return false;
    }

    require_login($course, true, $cm);

    if (!has_capability('mod/drmsocial:view', $context)) {
        return false;
    }

    if ($filearea !== 'content') {
        // Intro is handled automatically in pluginfile.php.
        return false;
    }

    $config = get_config('drmsocial');

    $drmsocial = $DB->get_record('drmsocial', ['id' => $cm->instance]);

    array_shift($args);

    $fs = get_file_storage();
    $relativepath = implode('/', $args);
    $fullpath = rtrim('/' . $context->id . '/mod_drmsocial/' . $filearea . '/0/' . $relativepath, '/');
    $file = $fs->get_file_by_hash(sha1($fullpath));

    if (!$file || $file->is_directory()) {
        return false;
    }

    $stored_file = $file;

    $path = $fs->get_file_system()->get_local_path_from_storedfile($file, false);
    $a = new \stdClass();
    $a->fullname = mb_strtoupper(fullname($USER), 'UTF-8');
    $a->email = $USER->email;

    $default = false;

    switch ($drmsocial->textlicense) {
        case 0:
            $default = true;
            break;
        case 1:
            $text = get_string('textlicense_defaultoption0', 'mod_drmsocial', $a);
            break;
        case 2:
            $text = get_string('textlicense_defaultoption1', 'mod_drmsocial', $a);
            break;
        case 3:
            $text = $drmsocial->textlicensecustom;
            break;
    }

    if ($default) {
        switch ($config->textlicense) {
            case 1:
                $text = get_string('textlicense_defaultoption0', 'mod_drmsocial', $a);
                break;
            case 2:
                $text = get_string('textlicense_defaultoption1', 'mod_drmsocial', $a);
                break;
            case 3:
                $text = $config->textlicensecustom;
                break;
        }
    }

    $variables = drmsocial_variables($USER);

    $text = trim(drmsocial_process_text($text, $variables));

    $text = utf8_decode($text);

    $filename = $stored_file->get_filename();
    $mimetype = $stored_file->get_mimetype();
    $cacheability = ' private,';
    $lifetime = 60;
    $immutable = '';
    $lastmodified = time();

    if ($forcedownload) {
        header('Content-Disposition: attachment; filename="' . $filename . '"');
    } else {
        header('Content-Disposition: inline; filename="' . $filename . '"');
    }

    header('Cache-Control:' . $cacheability . ' max-age=' . $lifetime . ', no-transform' . $immutable);
    header('Expires: ' . gmdate('D, d M Y H:i:s', time() + $lifetime) . ' GMT');
    header('Pragma: ');
    header('Content-Type: ' . $mimetype);
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s', $lastmodified) . ' GMT');
    header('Accept-Ranges: bytes');

    try {
        require_once(dirname(__FILE__) . '/vendor/autoload.php');

        // Text direction BOTTOM to TOP
        $s = $text;
        if (strpos($s, '(') !== false || strpos($s, ')') !== false || strpos($s, '\\') !== false || strpos($s, "\r") !== false) {
            $text = str_replace(array('\\', '(', ')', "\r"), array('\\\\', '\\(', '\\)', '\\r'), $s);
        } else {
            $text = $s;
        }
        $TextColor = sprintf('%.3F %.3F %.3F rg', 0 / 255, 0 / 255, 255 / 255);

        $pdf = new \setasign\Fpdi\Tfpdf\Fpdi();
        $pagecount = $pdf->setSourceFile($path);
        for ($i = 1; $i <= $pagecount; $i++) {
            $tplidx = $pdf->importPage($i);
            $sizes = $pdf->getTemplateSize($tplidx);
            $width = $sizes['width'];
            $height = $sizes['height'];
            $orientation = $sizes['orientation'];
            $pdf->addPage($orientation, [$width, $height]);
            $pdf->useTemplate($tplidx);
            // now write some text above the imported page
            $pdf->SetFont('Arial', '', 10);
            $pdf->SetTextColor(0, 0, 255);
            $pdf->SetXY(0, 0);
            $pdf->SetMargins(2, 2, 2);

            $k = 72 / 25.4;
            $h = $pdf->GetPageHeight();

            $x = $width - 2;
            $y = $height - 2;

            $s = sprintf('BT %.2F %.2F %.2F %.2F %.2F %.2F Tm (%s) Tj ET', 0, 1, -1, 0, $x * $k, ($h - $y) * $k, $text);
            $s = 'q ' . $TextColor . ' ' . $s . ' Q';
            $pdf->_out($s);
        }

        $content = $pdf->Output('S');

        if (drmsocial_pdftk_require()) {
            $tempdir = drmsocial_temp_dir();

            $file = new \mikehaertl\tmp\File($content, '.pdf', \mikehaertl\pdftk\Pdf::TMP_PREFIX, $tempdir);

            $pdfOptions = drmsocial_pdftk_options();
            $pdf = new \mikehaertl\pdftk\Pdf(null, $pdfOptions);
            $pdf->tempDir = $tempdir;
            $pdf->addFile($file->getFileName());
            $pdf->allow('Printing DegradedPrinting ScreenReaders ModifyAnnotations');
            $pdf->passwordEncryption(128);
            if ($tempcontent = $pdf->toString()) {
                $content = $tempcontent;
            }
        }
    } catch (Exception $e) {
        $filesize = $file->get_filesize();
        header('Content-Length: ' . $filesize);
        $fs->get_file_system()->readfile($file);
        die;
    }

    $filesize = strlen($content);
    header('Content-Length: ' . $filesize);

    echo $content;
    die;
}

function drmsocial_variables($user)
{
    $user = get_complete_user_data('id', $user->id);
    if (!$user) {
        print_error('nousersfound', 'moodle');
    }

    $a = new \stdClass();
    $a->username = strip_tags($user->username);
    $a->idnumber = strip_tags($user->idnumber);
    $a->firstname = strip_tags($user->firstname);
    $a->lastname = strip_tags($user->lastname);
    $a->email = strip_tags($user->email);
    $a->useremail = strip_tags($user->email);
    $a->icq = strip_tags($user->icq);
    $a->skype = strip_tags($user->skype);
    $a->yahoo = strip_tags($user->yahoo);
    $a->aim = strip_tags($user->aim);
    $a->msn = strip_tags($user->msn);
    $a->phone1 = strip_tags($user->phone1);
    $a->phone2 = strip_tags($user->phone2);
    $a->institution = strip_tags($user->institution);
    $a->department = strip_tags($user->department);
    $a->address = strip_tags($user->address);
    $a->city = strip_tags($user->city);
    $a->userimage = '';
    $a->fullname = strip_tags(fullname($user));

    if (!empty($user->country)) {
        $a->country = get_string($user->country, 'countries');
    } else {
        $a->country = '';
    }

    $url = $user->url;
    if (!empty($url) && strpos($url, '://') === false) {
        $url = 'http://' . $url;
    }
    $a->url = $url;

    $userprofilefields = drmsocial_get_user_profile_fields($user->id);
    foreach ($userprofilefields as $key => $value) {
        $key = 'profile_' . $key;
        $a->$key = strip_tags($value);
    }

    return $a;
}

function drmsocial_get_user_profile_fields($userid)
{
    global $CFG, $DB;

    $usercustomfields = new \stdClass();
    $categories = $DB->get_records('user_info_category', null, 'sortorder ASC');
    if ($categories) {
        foreach ($categories as $category) {
            $fields = $DB->get_records('user_info_field', array('categoryid' => $category->id), 'sortorder ASC');
            if ($fields) {
                foreach ($fields as $field) {
                    if (file_exists($CFG->dirroot . '/user/profile/field/' . $field->datatype . '/field.class.php')) {
                        require_once($CFG->dirroot . '/user/profile/field/' . $field->datatype . '/field.class.php');
                        $newfield = 'profile_field_' . $field->datatype;
                        $formfield = new $newfield($field->id, $userid);
                        if ($formfield->is_visible() && !$formfield->is_empty()) {
                            if ($field->datatype == 'checkbox') {
                                $usercustomfields->{$field->shortname} = (
                                $formfield->data == 1 ? get_string('yes') : get_string('no')
                                );
                            } else {
                                $usercustomfields->{$field->shortname} = $formfield->display_data();
                            }
                        } else {
                            $usercustomfields->{$field->shortname} = '';
                        }
                    }
                }
            }
        }
    }
    return $usercustomfields;
}

function drmsocial_process_text($text, $a = [])
{
    $text = format_text($text, FORMAT_PLAIN, ['noclean' => true, 'filter' => false]);

    $a = (array)$a;
    $search = array();
    $replace = array();
    foreach ($a as $key => $value) {
        $search[] = '{{' . strtoupper($key) . '}}';
        if (in_array($key, ['username', 'fullname', 'firstname', 'lastname'])) {
            $value = mb_strtoupper($value, 'UTF-8');
        }
        $replace[] = (string)$value;
    }

    if ($search) {
        $text = str_replace($search, $replace, $text);
    }

    $text = preg_replace('[\{(.*)\}]', "", $text);
    return format_text($text, FORMAT_PLAIN);
}

function drmsocial_pdftk_require(&$mform = null)
{
    global $CFG;

    $unexec = drmsocial_pdftk_options('unexec');
    $notfound = false;
    $exception = false;
    $exceptionmsg = '';

    try {
        require_once(dirname(__FILE__) . '/vendor/autoload.php');

        $pdfOptions = drmsocial_pdftk_options();

        $pdf = new \mikehaertl\pdftk\Pdf(null, $pdfOptions);
        $command = $pdf->getCommand();
        if (!$command->execute()) {
            $notfound = true;
        }
    } catch (Exception $e) {
        $exception = true;
        $exceptionmsg = $e->getMessage();
        debugging($e->getMessage());
    }

    if (!is_null($mform)) {
        if ($unexec) {
            $mform->addElement('static', 'pdftk-unexec', '', html_writer::div("Utilitário <strong>pdftk</strong> sem permissão de execução.", 'alert alert-warning'));
        }
        if ($notfound) {
            $mform->addElement('static', 'pdftk-notfound', '', html_writer::div("Não foi possível executar o utilitário <strong>pdftk</strong>.", 'alert alert-warning'));
        }
        if ($exception) {
            $mform->addElement('static', 'pdftk-exception', '', html_writer::div($exceptionmsg, 'alert alert-danger'));
        }
    }

    return !($unexec || $notfound || $exception);
}

function drmsocial_temp_dir()
{
    global $CFG;

    $tempdir = $CFG->tempdir . '/mod_drmsocial';
    if (!is_dir($tempdir)) {
        mkdir($tempdir);
    }

    return $tempdir;
}

function drmsocial_pdftk_options($var = 'pdfOptions')
{
    global $CFG;

    $unexec = false;

    $commandshell = $commandbin = false;

    $commandpath = $CFG->dirroot . '/mod/drmsocial/bin/pdftk';
    $commandsearch = (PHP_OS == 'WINNT') ? 'where' : 'which';
    if (!empty(shell_exec(sprintf("$commandsearch %s", escapeshellarg('pdftk'))))) {
        $commandshell = true;
    }

    if (file_exists($commandpath) && is_executable($commandpath)) {
        $commandbin = true;
    } else if (!$commandshell && file_exists($commandpath) && is_executable($commandpath)) {
        $unexec = true;
    }

    $pdfOptions = [];
    if (!$commandshell && $commandbin) {
        $pdfOptions['command'] = $commandpath;
        $pdfOptions['useExec'] = true;
    }

    if (strncasecmp(PHP_OS, 'WIN', 3) === 0) {
        $pdfOptions['useExec'] = true;
    }

    return $$var;
}

function drmsocial_dndupload_register()
{
    return [
        'files' => [
            [
                'extension' => 'pdf',
                'message' => get_string('dnduploaddrmsocial', 'mod_drmsocial')
            ]
        ]
    ];
}

function drmsocial_dndupload_handle($uploadinfo)
{
    // Gather the required info.
    $data = new stdClass();
    $data->course = $uploadinfo->course->id;
    $data->name = $uploadinfo->displayname;
    $data->intro = '';
    $data->introformat = FORMAT_HTML;
    $data->coursemodule = $uploadinfo->coursemodule;
    $data->file = $uploadinfo->draftitemid;

    $data->textlicense = 0;
    $data->revision = 1;

    return drmsocial_add_instance($data, null);
}
