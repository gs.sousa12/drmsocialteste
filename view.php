<?php

/**
 * DRM Social by Middag
 *
 * @package     mod_drmsocial
 * @copyright   2021 Middag Tecnologia (https://www.middag.com.br)
 * @author      Michael Meneses <michael@michaelmeneses.com.br>
 * @license     Commercial
 */

require_once(dirname(__FILE__) . '/../../config.php');

$cmid = required_param('id', PARAM_INT);
$forceview = optional_param('forceview', 0, PARAM_BOOL);

if (!$cm = get_coursemodule_from_id('drmsocial', $cmid, 0, false, MUST_EXIST)) {
    print_error('invalidcoursemodule');
}

if (!$course = $DB->get_record('course', array('id' => $cm->course), '*', MUST_EXIST)) {
    print_error('coursemisconf');
}

require_course_login($course, true, $cm);

$context = context_module::instance($cm->id);
require_capability('mod/drmsocial:view', $context);

$drmsocial = new \mod_drmsocial\drmsocial($cm->instance);

$title = $course->shortname . ': ' . format_string($drmsocial->name);

$PAGE->set_url('/mod/drmsocial/view.php', ['id' => $cm->id]);
$PAGE->set_title($title);
$PAGE->set_heading($course->fullname);

$completion = new completion_info($course);
$completion->set_module_viewed($cm);

$fs = get_file_storage();
$files = $fs->get_area_files($context->id, 'mod_drmsocial', 'content', 0, 'sortorder DESC, id ASC', false);
if (count($files) < 1) {
    echo $OUTPUT->header();
    echo $OUTPUT->notification(get_string('filenotfound', 'error'));
    echo $OUTPUT->footer();
    die;
} else {
    $file = reset($files);
    unset($files);
}

$path = '/' . $context->id . '/mod_drmsocial/content/' . $drmsocial->revision . $file->get_filepath() . $file->get_filename();
$fullurl = moodle_url::make_file_url('/pluginfile.php', $path, true);

if (!$forceview) {
    redirect($fullurl);
}

echo $OUTPUT->header();
echo $OUTPUT->heading(format_string($drmsocial->name), 2);
echo $OUTPUT->box(format_module_intro('drmsocial', $drmsocial, $cm->id), 'generalbox', 'intro');
echo $OUTPUT->box(html_writer::link($fullurl, get_string('download'), ['class' => 'btn btn-primary']), 'generalbox text-center', 'content');
echo $OUTPUT->footer();
