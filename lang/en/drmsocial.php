<?php

/**
 * DRM Social by Middag
 *
 * @package     mod_drmsocial
 * @copyright   2021 Middag Tecnologia (https://www.middag.com.br)
 * @author      Michael Meneses <michael@michaelmeneses.com.br>
 * @license     Commercial
 */

defined('MOODLE_INTERNAL') || die();

$string['dnduploaddrmsocial'] = 'Criar Arquivo PDF';
$string['drmsocial:addinstance'] = 'Add a new Arquivo PDF';
$string['drmsocial:view'] = 'View Arquivo PDF';
$string['eventpage_view'] = 'Arquivo PDF page viewed';
$string['modulename'] = 'Arquivo PDF';
$string['modulenameplural'] = 'Arquivos PDF';
$string['modulename_help'] = 'Este módulo é exclusivo para download de PDFs. Ao baixar o arquivo, é adicionado informações do usuário no PDF.';
$string['pluginadministration'] = 'Arquivo PDF administration';
$string['pluginname'] = 'Arquivo PDF';
$string['textlicense'] = 'Texto da Licença';
$string['textlicense_defaultoption0'] = 'Licenciado para {$a->fullname} - E-mail: {$a->email}.';
$string['textlicense_defaultoption1'] = '{$a->fullname} - {$a->email}.';
$string['textlicense_help'] = 'Texto a ser adicionado ao PDF para identificar o usuário emissor.';
$string['textlicense_optiondefault'] = 'Padrão ({$a->text})';
$string['textlicense_option1'] = 'Licenciado para [NOME COMPLETO] - E-mail: [EMAIL].';
$string['textlicense_option2'] = '[NOME COMPLETO] - [EMAIL]';
$string['textlicense_optioncustom'] = 'Utilizar campo Texto Customizado';
$string['textlicensecustom'] = 'Texto Customizado';
$string['textlicensecustom_help'] = 'Variáveis:
        {{FIRSTNAME}} - Nome
        {{LASTNAME}} - Sobrenome
        {{FULLNAME}} - Nome completo
        {{EMAIL}} - E-mail
        {{PROFILE_FIELDNAME}} - Campos personalizados do usuário';
