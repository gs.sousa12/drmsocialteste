<?php

/**
 * DRM Social by Middag
 *
 * @package     mod_drmsocial
 * @copyright   2021 Middag Tecnologia (https://www.middag.com.br)
 * @author      Michael Meneses <michael@michaelmeneses.com.br>
 * @license     Commercial
 */

defined('MOODLE_INTERNAL') || die;

require_once($CFG->dirroot . '/course/moodleform_mod.php');
require_once($CFG->libdir . '/filelib.php');
require_once($CFG->dirroot . '/mod/drmsocial/lib.php');

class mod_drmsocial_mod_form extends moodleform_mod
{
    public function definition()
    {
        global $CFG, $USER, $DB;

        $config = get_config('drmsocial');

        $mform =& $this->_form;

        $mform->addElement('header', 'general', get_string('general', 'form'));

        $mform->addElement('text', 'name', get_string('name'), array('size' => '48'));
        if (!empty($CFG->formatstringstriptags)) {
            $mform->setType('name', PARAM_TEXT);
        } else {
            $mform->setType('name', PARAM_CLEANHTML);
        }
        $mform->addRule('name', null, 'required', null, 'client');
        $mform->addRule('name', get_string('maximumchars', '', 255), 'maxlength', 255, 'client');

        moodleform_mod::standard_intro_elements();

        $filemanageroptions = array();
        $filemanageroptions['accepted_types'] = ['.pdf'];
        $filemanageroptions['maxbytes'] = 0;
        $filemanageroptions['maxfiles'] = 1;
        $filemanageroptions['mainfile'] = true;

        drmsocial_pdftk_require($mform);

        $mform->addElement('filemanager', 'file', get_string('selectfiles'), null, $filemanageroptions);

        $a = new stdClass();
        switch ($config->textlicense) {
            case 1:
                $a->text = get_string('textlicense_option1', 'mod_drmsocial');
                break;
            case 2:
                $a->text = get_string('textlicense_option2', 'mod_drmsocial');
                break;
            case 3:
                $a->text = $config->textlicensecustom;
                break;
        }

        $textlicenseoptions = [
            0 => get_string('textlicense_optiondefault', 'mod_drmsocial', $a),
            1 => get_string('textlicense_option1', 'mod_drmsocial'),
            2 => get_string('textlicense_option2', 'mod_drmsocial'),
            3 => get_string('textlicense_optioncustom', 'mod_drmsocial'),
        ];
        $mform->addElement('select', 'textlicense', get_string('textlicense', 'mod_drmsocial'), $textlicenseoptions);
        $mform->addHelpButton('textlicense', 'textlicense', 'mod_drmsocial');

        $mform->addElement('text', 'textlicensecustom', get_string('textlicensecustom', 'mod_drmsocial'));
        $mform->setType('textlicensecustom', PARAM_RAW);
        $mform->addHelpButton('textlicensecustom', 'textlicensecustom', 'mod_drmsocial');
        $mform->disabledIf('textlicensecustom', 'textlicense', 'neq', '3');

        $mform->addElement('hidden', 'revision');
        $mform->setType('revision', PARAM_INT);
        $mform->setDefault('revision', 1);

        $this->standard_coursemodule_elements();

        $this->add_action_buttons();
    }

    public function data_preprocessing(&$defaultvalues)
    {
        if ($this->current->instance) {
            $draftitemid = file_get_submitted_draft_itemid('file');
            file_prepare_draft_area($draftitemid, $this->context->id, 'mod_drmsocial', 'content', 0, array('subdirs' => true));
            $defaultvalues['file'] = $draftitemid;
        }
    }

    public function validation($data, $files)
    {
        $errors = [];

        if ($data['textlicense'] == 3 && empty($data['textlicensecustom'])) {
            $errors['textlicensecustom'] = get_string('required');
        }

        return $errors;
    }
}
